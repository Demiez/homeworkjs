/* I will take the galery from here https://www.mariosanchez.org/snapshots/ in a json file by parsing it
    function parseSanchez () {
    let g = [...document.querySelectorAll('figure')]
    let galery = []
    for (let item of g) {
        galery.push("https://www.mariosanchez.org" + item.innerHTML.split(" ")[1].substr(5).replace('"',''));
    }
    let json = JSON.stringify(galery);
    console.log(json)
}
parseSanchez()

 */
const images = function (){
    let request = new XMLHttpRequest();
    request.open("GET", "./galery.json", false);
    request.send(null);
    return JSON.parse(request.responseText);
}();

const previous = document.getElementById('previous')
const next = document.getElementById('next')
const mainImage = document.getElementById('main-image')
const prevImage = document.getElementById('min-previous-image')
const nextImage = document.getElementById('min-next-image')


let image = document.createElement('img');
image.setAttribute('src', images[0])
mainImage.appendChild(image);

let prevImageSmall = document.createElement('img');
prevImageSmall.setAttribute('src', images[images.length-1])
prevImage.appendChild(prevImageSmall);

let nextImageSmall = document.createElement('img');
nextImageSmall.setAttribute('src', images[1])
nextImage.appendChild(nextImageSmall);

let counter = 0;

next.onclick = () => {
    if (counter < images.length || counter == 0) {
        image.setAttribute('src', images[++counter])
    }
    if (counter == images.length) {
        counter = 0;
        image.setAttribute('src', images[counter])
    }
    checkState();
}
previous.onclick = () => {
    if (counter <= images.length) {
        image.setAttribute('src', images[--counter])
    }
    if (counter < 0) {
        counter = images.length - 1;
        image.setAttribute('src', images[counter])
    }
    checkState();
}

function checkState() {
    if (counter == 0) {
        prevImageSmall.setAttribute('src', images[images.length-1])
        nextImageSmall.setAttribute('src', images[counter+1])
        return
    }
    if (counter == images.length-1) {
        prevImageSmall.setAttribute('src', images[counter-1])
        nextImageSmall.setAttribute('src', images[0])
        return
    }
    if (counter > 0) {
        prevImageSmall.setAttribute('src', images[counter-1])
        nextImageSmall.setAttribute('src', images[counter+1])
        return
    }
}
