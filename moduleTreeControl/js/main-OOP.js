class Menu {
    constructor(targetElement, object) {
        this.targetElement = targetElement;
        this.object = object;
    }

}

class MenuOfSelects extends Menu {
    constructor(targetElement, object, name) {
        super(targetElement,object);
        this.name = name;
    }
    createTargetElement() {
        this.targetElement = document.createElement(`${this.targetElement}`)
        document.body.appendChild(this.targetElement).innerHTML = `${this.name}: `;
        return this;
    }
    draw(obj=this.object) {
        const select = document.createElement('select');
        this.targetElement.appendChild(select);
        if (Array.isArray(obj)) {
            for (let value of obj) {
                let option = document.createElement('option')
                select.appendChild(option).innerHTML = value;
            }

        } else {
            for (let key in obj) {
                let option = document.createElement('option')
                select.appendChild(option).innerHTML = key;
            }
        }

        select.onchange = (e) => {
            this.searchNext(e,obj);
        };
    }
    searchNext(e,obj) {
        for (let [key,value] of Object.entries(obj)) {
            if (key === e.target.value) {
                let children = e.target.parentElement.children;
                for (let i = 0; i < children.length; i++) {
                    if(children[i] === e.target) {
                        while (children[i].nextSibling != null) {
                            children[i].nextSibling.remove()
                        }
                    }
                }
                this.draw(value);
            }
        }
    }
}

let carsMenu = new MenuOfSelects(
    'label',
    {
        minivan: {
            mercedes: ["b250", "2102"],
            volkswagen: ["touran"]
        },
        sedan: {
            bmw: ["328", "530"],
            lada: ["2107"]
        },
    },
    'Cars'
);
carsMenu.createTargetElement().draw();

let racesMenu = new MenuOfSelects(
    'label',
    {
        human: {
            Barbarian: {
                abilities: ["Mortal Blow", "War Cry"],
                weapons: ["Two-handed swords", "Two-handed axes"]
            },
            Imperial: {
                abilities: ["Execute", "High Command"],
                weapons: ["One-handed swords", "One-handed axes", "Shields"]
            },
            Highlander: {
                abilities: ["Long shot", "Sniper stance"],
                weapons: ["Long bows", "Short swords"]
            }
        },
        orc: {
            bloodClan: {
                abilities: ["Bloodlust", "Lacerate"],
                weapons: ["One-handed axes", "Two-handed axes"]
            },
            darkMoonClan: {
                abilities: ["Night howl", "Multiple strikes"],
                weapons: ["One-handed axes", "Thrown axes"]
            },
            redSpearClan: {
                abilities: ["Long throw", "Penetrate"],
                weapons: ["Spears", "One-handed axes"]
            },
        },
        elf: {
            woodElfs: {
                abilities: ["Fast shot", "Tame beast"],
                weapons: ["Elven bows", "Daggers"]
            },
            darkElfs: {
                abilities: ["Cheap shot", "Blood wound"],
                weapons: ["Daggers", "Thrown daggers"]
            },
            highElfs: {
                abilities: ["Fireball", "Magic Shield"],
                weapons: ["Staves", "Wands"]
            },
        },
    },
    'Races'
);
racesMenu.createTargetElement().draw();
