class Menu {
    constructor(targetElement,object) {
        this.targetElement = targetElement
        this.object = object
    }

    draw() {

        let drawMenu = (obj) => {
            const select = document.createElement('select');
            label.appendChild(select);
            if (Array.isArray(obj)) {
                for (let value of obj) {
                    let option = document.createElement('option')
                    select.appendChild(option).innerHTML = value;
                }

            } else {
                for (let key in obj) {
                    let option = document.createElement('option')
                    select.appendChild(option).innerHTML = key;
                }
            }

            select.onchange = (e) => {

                let search = (obj) => {
                    for (let [key,value] of Object.entries(obj)) {
                        if (key === e.target.value) {
                            // console.log(e.target)
                            for (let i = 0; i<e.target.parentElement.children.length; i++) {
                                if(e.target.parentElement.children[i] === e.target) {
                                    while (e.target.parentElement.children[i].nextSibling != null) {
                                        e.target.parentElement.children[i].nextSibling.remove()
                                    }
                                }
                            }
                            // console.log(e.target.parentElement.children)
                            drawMenu(value);
                        }
                    }
                };

                search(obj);
            };
        };




        const select = document.createElement('select');
        this.targetElement.appendChild(select);
        if (Array.isArray(this.object)) {
            for (let value of this.object) {
                let option = document.createElement('option')
                select.appendChild(option).innerHTML = value;
            }

        } else {
            for (let key in this.object) {
                let option = document.createElement('option')
                select.appendChild(option).innerHTML = key;
            }
        }

        select.onchange = (e) => {

            let search = (obj) => {
                for (let [key,value] of Object.entries(obj)) {
                    if (key === e.target.value) {
                        // console.log(e.target)
                        for (let i = 0; i<e.target.parentElement.children.length; i++) {
                            if(e.target.parentElement.children[i] === e.target) {
                                while (e.target.parentElement.children[i].nextSibling != null) {
                                    e.target.parentElement.children[i].nextSibling.remove()
                                }
                            }
                        }
                        // console.log(e.target.parentElement.children)
                        drawMenu(value);
                    }
                }
            };

            search(this.object);
        };
    }
}



const label = document.createElement('label');
document.body.appendChild(label).innerHTML = 'Cars: ';

var cars = {
    minivan: {
        mercedes: ["b250", "2102"],
        volkswagen: ["touran"]
    },
    sedan: {
        bmw: ["328", "530"],
        lada: ["2107"]
    },
};

let carMenu = new Menu(label,cars);
console.log(carMenu)
carMenu.draw();

