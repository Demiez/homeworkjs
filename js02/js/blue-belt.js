var house = {
  block: 0,
  flatFloor: 0,
  floorNumber: null,
  flatsOnTheFloor: null,
  flatNumber: null,
  infoChecked: null,
  checkInfo: function() {
    if (isNaN(this.floorNumber) || isNaN(this.flatsOnTheFloor) || isNaN(this.flatNumber)) {
        this.infoChecked = false;
        document.getElementById("result").style["color"] = "#f00";
        document.getElementById("result").value = "Sorry, but check your info once more!";
    } else {
        document.getElementById("result").style["color"] = "#000";
        this.infoChecked = true;
    }
  },
  findLocation: function() {
      debugger;
      var flatsInBlock = this.floorNumber * this.flatsOnTheFloor;
      if (this.flatNumber <= flatsInBlock) {
        this.block = 1;
        this.flatFloor = Math.ceil (this.flatNumber / this.flatsOnTheFloor);
        document.getElementById("result").value = "Your flat is located in " + this.block + " block on the " + this.flatFloor + " floor.";
        return; // fix, without it the function continued with % and received a 0 value for the cases 9x4 and 36flat, or 5x5 25 flat
      }
      this.block = Math.ceil (this.flatNumber / flatsInBlock);
      this.flatFloor = Math.ceil ((this.flatNumber % flatsInBlock) / this.flatsOnTheFloor);
      document.getElementById("result").value = "Your flat is located in " + this.block + " block on the " + this.flatFloor + " floor.";
    }
};

function init() {
    let checkButton = document.getElementById("checker");
    checkButton.onclick = startChecking; // Когда на онклик цепляю вызов метода в объекте, тот почему то стартует сразу при парсинге самого скрипта. Почему?
}

function startChecking() {
    house.floorNumber = +floorNumberField.value;
    house.flatsOnTheFloor = +flatsOnTheFloorField.value;
    house.flatNumber =  +flatNumberField.value;
    house.checkInfo();
    if (house.infoChecked) house.findLocation();
}

window.onload = init;
