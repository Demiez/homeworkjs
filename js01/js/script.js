//declaring variables
var date; // Current date (Monday - Sunday)
var bucks; // How much money does user have?
var drunkPartyDay = 5; // yeah, it's Friday (5th day), still changeable to any other date

tell.onclick = () => { // call a function onclick
  bucks = +money.value;
  date = day.value;
  var result;

  // Validation if user has enough money and has entered 'number' and not a word
  if (bucks <= 30) {
    return alert('No way you can afford drunk party, get more money')
  } else if (isNaN(bucks)) {
    return alert ("That's not money! Try to enter an exact value!")
  }

  // Each day receives its own numerical value
  if (date == "monday" || date == "Monday") {
    date = 1;
  } else if (date == "tuesday" || date == "Tuesday") {
    date = 2;
  } else if (date == "wednesday" || date == "Wednesday") {
    date = 3;
  } else if (date == "thursday" || date == "Thursday") {
    date = 4;
  } else if (date == "friday" || date == "Friday") {
    date = 5;
  } else if (date == "saturday" || date == "Saturday") {
    date = 6;
  } else if (date == "sunday" || date == "Sunday") {
    date = 7;
  } else {
    return alert("You are already drunk if you don't know what day is today...") // validation if user has entered correct title of the current day
  }

  result = drunkPartyDay - date; // Calculating and displaying result
  if (result >= 2) {
    return alert("You need to wait " + result + " days! And you have " + bucks + " bucks for it!");
  } else if (result == 1) {
    return alert("Your party is tomorrow! Get ready to spend your " + bucks + " !");
  } else if (result == 0) {
    return alert("Damn, your party is today! Hurry up and spend your "+bucks+" !");
  } else {
    return alert("You have missed this week's drunk party!")
  }
}
