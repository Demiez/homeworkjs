var credentials = {
  login: "admin",
  password: "qwerty"
}
var log;
var pass;

function init() {
  var logButton = document.getElementById("logButton");
  logButton.onclick = passwordCheck;
}

function passwordCheck () {
  log = loginField.value;
  pass = passwordField.value;

  // in case it is a second attempt we just return values to none
  document.getElementById("success").style["display"] = "none";
  document.getElementById("error").style["display"] = "none";
  //#####

  if (log == credentials.login && pass == credentials.password) {
    document.getElementById("message").style["background"] = "#75e154";
    document.getElementById("success").style["display"] = "block";
    return;
  } else {
    document.getElementById("message").style["background"] = "#f9310a";
    document.getElementById("error").style["display"] = "block";
    return;
  }
}

window.onload = init;
