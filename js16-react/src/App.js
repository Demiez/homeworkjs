import React, {Component} from 'react';
import './App.css';
import Password from './login/Password'
import ConfirmPassword from './login/ConfirmPassword'
import ComparePasswords from './login/ComparePasswords'

var password
var confirm

class App extends Component {

    componentDidMount() {
        this.setState({
            reloaded: true
        })
    }

    render () {

        return (
            <div className="App">
                <Password ref={(element) => password = element} />
                <ConfirmPassword ref={(element) => confirm = element} />
                <ComparePasswords inputs={[password, confirm]} />
            </div>
        )
    }
}

export default App;

