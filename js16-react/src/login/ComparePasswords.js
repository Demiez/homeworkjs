import React from 'react'

export default class ComparePasswords extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            innerText: ''
        }
    }

    render () {
        const [password, confirm] = this.props.inputs

        const comparePasswords = (one, two) => {
            if (one != "" && two !="" && one === two) {
                this.setState({
                    innerText: 'YES, passwords match!'
                })
            } else {
                this.setState({
                    innerText: 'NO, passwords should match!'
                })
            }
        }
        return (
            <div className='comparePasswords'>
                <p>{this.state.innerText}</p>
                {/*<button onClick={() => console.log(this.props)}>Check!</button>*/}
                <button onClick={() => comparePasswords(password.state.password, confirm.state.password)}>Check!</button>
            </div>
        )
    }
}
