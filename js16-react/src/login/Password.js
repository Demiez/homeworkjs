import React, {Component} from 'react'
import validate from "./Validator";

export default class Password extends Component {
    state = {
        password: '',
        class: null
    }

    render () {
        let password = this.state.password

        const checkPassword = (e) => {
            this.setState(
                {password: e.target.value},
                () => this.setState({class: validate(this.state.password)}))
        }

        return (
            <div className='password'>
                <input value = {password}
                       className = {(this.state.class === 'ok' && password) ? 'ok' :
                           (this.state.class === 'notok' && password) ? 'notok' : null}
                       onChange = {e => checkPassword(e)}
                />
                Password
            </div>
        )
    }
}
