export default function validate (password) {
    const regEx = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})")

    // We use medium strong validation 6 symbols with 1 number or 1 uppercase

    return password.match(regEx) ? 'ok' : 'notok'
}
