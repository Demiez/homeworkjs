let starWarsInfo = (page) => {
    let div = document.createElement('div');
    document.body.appendChild(div);

    function myFetch (url) {
        if (window.localStorage.getItem(url)) {
            return Promise.resolve(JSON.parse(window.localStorage.getItem(url)));
        } else {
            return fetch(url).then(res => res.json());
        }
    }


    function createTable(page, parentElem, myFetch) {
        let table = document.createElement('table');
        parentElem.appendChild(table);

        myFetch(page)
            .then(json => parse(json, myFetch));


        function parse(json, myFetch) {
            function checkForName (url,myFetch) {
                return myFetch(url)

                    .then(function (json) {
                        for (let [key, value] of Object.entries(json)) {
                            if (key == "name" || key == "title") {
                                return value;
                            }
                        }
                    })
            }

            for (let [key, value] of Object.entries(json)) {
                let tr = document.createElement('tr');
                let td01 = document.createElement('td');
                let td02 = document.createElement('td');
                let td03 = document.createElement('td');
                td01.innerHTML = key;
                table.appendChild(tr).appendChild(td01);

                if (typeof (value) == 'object') {
                    td02.innerHTML = "";
                    tr.appendChild(td02);
                    parse(value, myFetch);
                } else {
                    let a = value.toString();
                    if (a.startsWith("https://swapi.co/api/")) {
                        checkForName(value, myFetch).
                        then(function (r) {
                            let button = document.createElement('button');
                            button.innerText = `${r}`;
                            let editButton = document.createElement("button");
                            editButton.innerText = "edit";
                            tr.appendChild(td02);
                            tr.appendChild(td03);
                            td02.appendChild(button);
                            td03.appendChild(editButton);
                            button.addEventListener("click", () => {
                                createTable(value, td02, myFetch);
                                td02.removeChild(button);
                            });
                            editButton.addEventListener("click", () => {
                                myFetch(value)
                                    .then(json => {
                                        let form = new Form(td03,json,() => window.localStorage.setItem(value,JSON.stringify(form.data)), () => console.log('All inputs are canceled.'))
                                    }).then(r =>{
                                    let findButton = document.createElement('button');
                                    findButton.innerText = 'Reload page and find new button!!! \n(First press OK at least once or it wont work!!!)';
                                    td03.appendChild(findButton);
                                    findButton.onclick = () => {
                                        if (window.localStorage.length > 0) {
                                            location.reload()
                                        }
                                    }
                                });
                                td03.removeChild(editButton);
                            });
                        });
                    } else {
                        td02.innerHTML = value;
                        tr.appendChild(td02);
                    }
                }
            }
        }
    }
    createTable(page,div,myFetch);
};

starWarsInfo('https://swapi.co/api/starships/12/');

