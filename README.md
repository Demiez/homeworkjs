# homeworkJS
### JS01
1) drunk calculator (index.html)<br>
2) create login and password form, which after pressing **login** button must check if login and password are correct. In case of success - **green** `div` with **success**, if not - **red** `div` with **error** (index01.html).

### JS02
1) mini tasks 01-22 (homework 01-22.html)<br>
2) blue belt task (blue-belt.html) - Create a calculator to check in what block and on what floor is a flat located.

### JS03
1) mini tasks 01-14 + additional task (homework.html)<br>
2) blue belt task (blue-belt.html) - use object (used from the beginning)<br>
3) black belt task (black-belt.html) - all the game scissors into 1 expression (reached 2 expressions)

### JS04
1) theory 04 tasks (theorytasks04.html)<br>
2) homework 04 tasks (homework04.html)<br>
3) blue belt task (blue-belt04.html) - create triangle<br>
4) black belt task (black-belt04.htm) - create game Fortune Teller

### JS05
1) theory 05 tasks (theorytasks05.html)<br>
2) homework 05 tasks (homework05.html)<br>
3) blue belt task (blue-belt05.html) - create constructor for object with tags<br>
4) updated blue-belt05recursion.html - constructor with recursion- 10 strings of code only<br>

### JS06
1) theory 06 tasks (theorytasks06.html)<br>
2) homework 06 tasks (homework06.html)<br>

### JS07
1) countries-upd (countries-upd.html)<br>
2) homework 01-03 tasks (homework07 01-03.html) - multiplyTable created with DOM methods with highlighting of row and column<br>
3) homework 04 task (homework07 04.html) - calculator created with DOM methods<br>
3) homework 05 task (homework07 05.html) - live calculator<br>

### JS08
1) homework 08 tasks (homework08.html) - constructor updated to any depth with recursion and takes "attrs" into consideration<br>

### JS09
1) theory 09 tasks (theorytasks09-p1.html) - DOM2 and event handlers, stopPropagation and event bubbling, capturing<br>
2) theory 09 tasks (theorytasks09-p2.html) - closures, private methods, getter/setter pattern, call & apply (built-in methods of functions), prototype<br>
3) call-apply-bind difference (call-apply-bind.html)<br>
4) homework 09 01-03 tasks (homework09 01-03.html) - makeProfileTimer, makeSaver, Final Countdown<br>
5) homework 09 04 task (homework09 04.html) - myBind<br>

### JS10
1) socketchat with smiles, pics and videos(use npm install for modules, check server settings, run server (node index.js), run index.html)

### JS11
1) starwars parsing from https://swapi.co,  includes fetch, tables with buttons to surf through content (starwars new table.html & starwars same element)<br>
2) theory of XMLHttpRequest and Promise (theory XMLHttpRequest.html & theory promise.html)<br>
3) Homework 11 tasks 01,02 (in starwars new table.html & starwars same element)<br>
4) Homework 11 tasks 03,04 (homework11.html) task03(myfetch) - promisification of fetch with use of XMLHttpRequest; task04(race) - Promise.race of myfetch and randomized delay.<br>

### JS12
1) chat stage 1-4 with XMLHttpRequest, promise, then (homework12 stage 1-4.html)<br>
2) final version of chat with async, await, fetch for stages 5-6 (homework12 async fetch)<br>

### JS13
1) Traffic Light (traffic-light stage1-2.html) - Svetofor stage 1-2<br>
2) domEventPromise (domEventPromise.html) - button<br>
3) Pedestrian Traffic Light with Promise.race (PedestrianTrafficLight.html)<br>
4) final Pedestrian and Traffic together (PedestrianAndTrafficTogether.html)

### JS14
1) form constructor with validation (./ver1.0/index.html) --> with id/class
2) fixed, no id/class use (index.html) --> multiple forms available
2) power of the form: <br>
 change password - done + changePasswordForm + validation<br>
 Starwars - done<br>
 Power of closures - done<br>
 Starwars localStorage - done<br>
 Cached Promise - done + added a button,which appears on main page to reload when there are items in Local storage<br>
 
### JS15
1) selects and highlights element (circle, line, circles from graffiti)<br>
2) deletes selected element<br>
3) added rectangle

### Module - TreeControl
1) Done OOP (main-OOP.js is used)
2) Checked on multiple objects(cars, races)

### JS16-react
1) password validation done<br>
2) compare both inputs done<br>
 

